
# Display ST7789 test app

## How to install VSCode with Platformio
https://platformio.org/install/ide?install=vscode

## Required libraries
https://github.com/adafruit/Adafruit_BusIO
https://github.com/adafruit/Adafruit-GFX-Library

## Connecting a 240×240 TFT display with ST7789 controller with a NodeMCU ESP8266 or an Arduino Nano
https://thesolaruniverse.wordpress.com/2019/12/24/connecting-a-240x240-tft-display-with-st7789-controller-with-a-nodemcu-esp8266-or-an-arduino-nano/comment-page-1/

